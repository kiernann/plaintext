# plaintext.us
* site: https://plaintext.us
* twitter: [@plaintext_us](https://twitter.com/plaintext_us)
* founder: [Kiernan Nicholls](https://k5cents.com)

# philosophy
This project was initially inspired after reading [this website](https://motherfuckingwebsite.com/) (and [the successor](https://bestmotherfucking.website/)). Today's journalism is  a perverted evolution of the traditional print media; advertising, tracking, and excessive web design have made the consumption of news almost unbarebale on a full sized desktop, let alone a mobile device.

Plaintext will give you the news. It will give it to you in plain language and plain text.
